---
title: Java8的Lambda表达式
date: 2019-08-30 21:46:34
tags: Java
---

Java 8版本是现在Java最流行的一个版本。主要改进Java在面向对象的基础上对函数式编程的支持。本文将展示Lambda的基本语法。

<!-- more -->

### 为什么使用Lambda表达式？

我们使用Java的集合经常会定义比较器来进行集合排序。比如要按照字符串长度对字符进行排序，实现一个自定义的Comparator对象并传递给方法进行排序，如：
``` java
class LengthComparator implements Comparator<String> {

    public int compare(String firstStr, String secondStr) {
    
        return Integer.compare(firstStr.length(), secondStr.length());
    }
}

Arrays.sort(strings, new LengthComparator());
```
编写这一段用于比较的代码片段，封装在自定义的Comparator里。Arrays.sort方法会在适当时机就会调用此代码片段，对strings数组进行排序。
那么，这个适当时机，是什么时候呢？它可能是某个按钮被点击时，也可能是某个新线程被启动时，如：
``` java
class MyRunner implements Runnable {
    
    public void run() {
        for(int i = 0; i < 1000; i++) {
            System.our.println("do work");
        }
    }    
    ...
}

new Thread(new MyRunner()).start();
```
把一段代码块传递 - 线程池，排序方法。希望在适当时机需要时，调用这段代码来进行相关工作或排序。

在java8以前，想要传递代码块很不容易。我们只能把代码块写在一个特殊的类里, 或者使用匿名内部类的方式，实例化一个类对象来传递这段代码。

### Lambda的语法

对于字符串排序，需要明确两个问题
1. 处理传入的参数类型是什么？是什么数据类型？
2. 使用什么代码片段来进行处理？

于是，Lambda的表达式可以:
``` java
(String str1, String str2) -> Integer.compare(str1.length(), str2.length());
```

如果可以推断Lambda的参数类型，可以省略类型，如：
``` java
Comparator<String> com = (str1, str2) -> Integer.compare(str1.length(), str2.length());
```


