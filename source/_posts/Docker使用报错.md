---
title: Docker安装后报Cannot connect to the docker daemon at ...
date: 2019-07-31 23:03:34
tags: Docker
---

Docker安装后报错：Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
<!-- more -->

### 解决方式执行以下命令

``` bash
$ systemctl daemon-reload
$ sudo service docker restart
$ sudo service docker status (should see active (running))
```