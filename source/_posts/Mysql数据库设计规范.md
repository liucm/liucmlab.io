---
title: Mysql数据库设计规范
date: 2019-07-31 23:03:34
tags: Mysql
---

一般使用Mysql数据比较多，但是有点小伙伴很少考虑Mysql设计上的细节问题，像SQL语句的规范、索引上的规范等等，本文经过整理后得到一些常用的Mysql数据库设计规范。

<!-- more -->

### Mysql命令规范

``` text
- 所有数据库表命名都用小写字母、并用下换线分割
- 所有数据库表名尽量不要使用Mysql关键字命名、能看到名称大概了解表是做什么的
- 临时表以tmp_为前缀、日期后缀区分，备份表以bak_为前缀、日期后缀
- 相同的数据字段，在所有表中的列明和类型尽量保持一致
```

### 基本设计规范

``` text
- 大部分情况尽量使用InnoDB引擎，InnoDB支持事务、行级锁、性能更好
- 数据库尽量使用utf8mb4的字符集，兼容性好，避免乱码和索引创建失败的问题
- 表和字段都尽量加上注释，方便以后维护
- 尽量不要使用存储过程、视图、触发器、Event。可以减少数据库资源的占用、将计算转移到服务层
- 单表的数据量尽量控制在500万以内，数据量过大影响Mysql性能和维护，更多的数据可以对数据库进行分库分表拆分，控制单表的数据量
- 尽量不要使用Mysql的分区功能，分区是物理上多个文件，逻辑上一个文件，查询效率很低。可以使用中间件mycat、sharding-jdbc等进行物理分区
- 尽量减少表的宽度、数据冷热数据进行分离、保证每个表都有主键
  - Mysql列数限制在为4096列，每一行数据不超过65535字节；宽度越大、加载的内存就越大，IO消耗也就越大
  - 经常用的数据放在一起，不常用的数据分离出去，可以极大提高效率
  - 主键可以更好的利用索引，提高查询效率
- 表中尽量不用外键，通过程序来维护，外键导致表与表之间耦合，涉及相关的表，十分影响性能且容易造成死锁
```

### 字段涉及规范

``` text
- 优先选择符合业务最小的存储类型，节省数据空间减少IO消耗
- 字段定义使用Not Null，可以提供一个默认值
  - null值索引很难优化，null值更占用空间，需要额外空间标识
  - null值操作只能采用is null或is not null
  - 不能采用=、in、<、<>、not in、！=操作符，不方便操作，where name != 'abc', 不会查询出name为null的数据
- 尽量不使用Text、Blob类型
  - Mysql使用Text、Blob类型就不使用内存临时表而使用磁盘临时空间、性能差浪费磁盘和内存空间。
  - 降低数据库内存命中率
  - 可以单独建立扩展表使用
- 尽量不使用Enum、用Tinyint
  - 修改Enum值时，需要通过alter修改表
  - order by的操作效率低
- 尽量不要使用小数，尤其涉及金钱等精度要求较高的类型
  - 最好使用整数，小数存储有精度的误差，导致金额对不上
- 时间尽量使用Timestamp或者Datetime类型存储时间，不要使用String类型
  - 使用String无法使用日期函数进行比较比较
  - 用String会占用更多的空间
```

### 索引设计规范

``` text
- 表的索引不要太多，尽量5个以下，索引增加查询效率同时也会降低插入和更新的效率
  - 索引太多会影响Mysql优化器，优化器消耗时间增加，反而降低查询到的性能
- 更新频繁的列、或者列值区分度不高的列(如：性别，只有男、女、未知)不要加索引
- 联合索引，把区分度高的放在左侧，尽量遵循最左索引原则
  - 区分度高的列放在最左侧
  - 查询频繁的列放在最左侧
  - 字段长度小的放在最左侧
```

### SQL语句规范

``` text
- 尽量不要使属性隐式转换，隐式转换使索引失效
  - SELECT name FROM project WHERE id = `'1000'`; id为整型，用`1000`代替
- 程序中使用预编译语句操作数据库，减少SQL编译时间同时防止SQL注入
- insert语句尽量包含字段
  - 如：INSERT INTO test values('1', 'abc');应该INSERT INTO test(id, name) values('1', 'abc')
- 尽量不要使用%开头的模糊查询
  - not、!=、<>、not in、not like以及%开头等，会是查询不走索引而全变扫描
- 联合索引只能有一列进行范围查询
  - a1,a2,a3联合索引，a1范围查询，a2，a3列不会走索引，可以把a1放在最后
- Where语句后的字段尽量不要使用函数或表达式，否则不会走索引
  - SELECT id FROM t WHERE num/2=100，这将导致引擎放弃使用索引而进行全表扫描，可改为：SELECT id FROM t WHERE num=100*2
  - SELECT id FROM t WHERE substring(name,1,3)='abc'--name以abc开头的id，应改为:SELECT id FROM t WHERE name LIKE 'abc%'
  - SELECT id FROM t WHERE datediff(day,createdate,'2005-11-30')=0--'2015-11-30'生成的id，应改为:SELECT id FROM t WHERE createdate>='2015-11-30' AND createdate<'2015-12-1'
- 大表不要使用子查询，不使用JOIN查询
- 避免JOIN关联太多表
- OR条件可以换成IN查询
- 尽量减少与数据库交互次数
```