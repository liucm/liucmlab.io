---
title: Mysql导入导出命令详解
date: 2019-08-13 20:10:29
tags: Mysql
---

Mysql导入导出数据库经常使用，记录一下以后方便查找

<!-- more -->

### 导出数据和表结构

> 使用mysqldump可以加上--default-character-set=utf8 

``` text
mysqldump -u用户名 -p密码 --default-character-set=utf8  数据库名 > 数据库名.sql

mysqldump -uroot -p123456 --default-character-set=utf8 test > test.sql

mysqldump -uroot -p test > test.sql
```

### 只导出表结构
``` text
mysqldump -u用户名 -p密码 -d 数据库名 > 数据库名.sql

mysqldump -uroot -p123456 -d test > test.sql
```

### 导入数据库

#### 使用mysql命令导入
``` text
mysql -u用户名 -p 数据库名 < 数据库名.sql

mysql -uroot -p123456 test < test.sql
```

#### 使用source命令导入
``` text
use test;                     # 使用已经创建的数据库
source /home/test/test.sql    # 导入备份的sql文件
```

### 导出远程数据库
``` text
mysqldump -h远程服务器ip地址 -P端口号  -u用户名 -p密码 --default-character-set=utf8 test > /home/test/test.sql

mysqldump -h192.168.22.11 –P3306 -uroot -ppassword --default-character-set=utf8 test > /home/test/test.sql
```

